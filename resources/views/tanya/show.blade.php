@extends('adminlte.master')

@section('content') 
	
<div class="row g-4">
	<div class="col-sm-12 col-md-6 col-xl-12">
		
		<div class="d-flex w-100 align-items-center justify-content-between">
			<div>
				
				<h4>{{$tanya->judul}}</h4>				
				<small>{{Auth::user()->name}} opened this issue on <?=$formatDate = date('M d, Y', strtotime($tanya->tgljam))?> | <i class="fa fa-times"></i> 2 Answers</small>
				<div>
					<small><b>Category :</b> 
						@forelse ($kategori as $item)
							@if ($item->tanya_id == $tanya->id)
								<div class="btn btn-secondary btn-sm rounded-pill my-2" style="cursor:none; font-size:10px">
									{{$item->namakategori}}
								</div>
							@endif
						@empty
							No kategori
						@endforelse
					</small>
				</div>
			</div>	
			
			@if($tanya->isclosed == 0)				
				<button type="button" class="btn btn-sm btn-success rounded-pill m-2" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
			@else 
				<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
			@endif
		</div>								
		<hr />
															
		<div class="alert alert-info alert-dismissible fade show" role="alert">
			<div>
				<img class="rounded-circle me-lg-2" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
				<small>{{Auth::user()->name}} question on {{$formatDate}} </small>
				<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
			</div>
			<hr />
			<div class="alert alert-light" role="alert">
				{!! $tanya->pertanyaan !!}								
			</div>	
			<a class="btn btn-primary" href="/showask/{{$tanya->id}}" role="button"><i class="fas fa-location-arrow"></i> Go To Post</a>
			<a class="btn btn-outline-success" href="/saveitem/{{$tanya->id}}" role="button"><i class="fas fa-bookmark"></i>  Save</a>																
		</div>
		
		<hr />
		<h5>{{count(App\Tanya::find($tanya->id)->jawab)}} Answers</h5>
		
		@forelse (App\Tanya::find($tanya->id)->jawab as $key => $value)
			<div class="alert alert-secondary">
				<div>
					@if (!App\User::find($value->user_id)->profile->avatar == null)
						<img src="{{ asset('images/' . App\User::find($value->user_id)->profile->avatar) }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">
					@else
						<img src="{{ asset('images/default-profile.png') }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">            
					@endif
					<small class="mx-2">{{App\User::find($value->user_id)->name}} comment on {{$formatDate = date('M d, Y', strtotime($value->tgljam))}}</small>
					
				</div>
				<hr />
				@if ($value->issolutions != 0)
					<div class="btn btn-success rounded-pill mb-3"><i class="fa fa-check"></i> Solution</div>
				@endif
				<div class="alert alert-light" role="alert">
					{!!$value->jawaban!!}
				</div>	
				
			</div>
		@empty
			<p>No Comment Available</p>
		@endforelse
		
	</div>
									
</div>
	
@endsection

@push('scripts')
	
@endpush