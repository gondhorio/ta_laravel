@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">
		<div class="w-100 align-items-center justify-content-between">
			<center>
				<h1>My Q & A</h1>
			</center>
			<a href="{{ route('tanya.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Buat Pertanyaan</a>
		</div>
		<hr/>
			@forelse ($tanya as $key=>$value)
			
			<div class="row g-4 mb-3">
				<div class="col-sm-12 col-md-6 col-xl-12">
					
					<div class="d-flex w-100 align-items-center justify-content-between">
						<div>
							<h4 class="d-inline">{{$value->judul}}</h4>	<br>			
							<small>
								You opened this issue on <?=$formatDate = date('M d, Y', strtotime($value->tgljam))?> | 
								@if($value->isclosed == 0)				
									<i class="fa fa-times"></i>
								@else 
									<i class="fa fa-check"></i>
								@endif
								 {{count(App\Tanya::find($value->id)->jawab)}} Answers
							</small>
							<div>
								<small><b>Category :</b> 
									@forelse ($kategori as $item)
										@if ($item->tanya_id == $value->id)
											<div class="btn btn-secondary btn-sm rounded-pill my-2" style="cursor:none; font-size:10px">
												{{$item->namakategori}}
											</div>
										@endif
									@empty
										No kategori
									@endforelse
								</small>
							</div>
							<div class="d-flex my-2">
								<a href="/tanya/{{$value->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Detail</a>
								<a class="btn btn-info btn-sm mx-2" href="/tanya/{{$value->id}}/edit"><i class="bi bi-pencil-square"></i>Edit</a>
    
								<form action="{{ route('tanya.destroy', ['tanya' => $value->id] ) }}" method="POST">
									@csrf
									@method('DELETE')
									
									<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"> </i> Delete</button>
								</form>
							</div>
						</div>	
		
						@if($value->isclosed == 0)				
							<button type="button" class="btn btn-sm btn-success rounded-pill m-2" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
						@else 
							<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
						@endif
					</div>								
					<hr />
																		
					{{-- <div class="alert alert-info alert-dismissible fade show" role="alert">
						<div>
							<img class="rounded-circle me-lg-2" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
							<small>{{Auth::user()->name}} question on {{$formatDate}} </small>
							<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
						</div>
						<hr />
						<div class="alert alert-light" role="alert">
							{!! $value->pertanyaan !!}								
						</div>																	
					</div> --}}
				</div>	
			</div>
			@empty
				<p>No Data</p>
			@endforelse              
	</div>
	
@endsection

@push('scripts')
	
@endpush