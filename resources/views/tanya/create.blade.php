@extends('adminlte.master')

@section('content') 
	<div align="center">		
		<h5>Posting Pertanyaan</h5>
		<hr />
	</div>
	
	<form action="{{ route('tanya.index') }}" method="post">
	@csrf
	<div class="card-body">
		<div class="form-group">
			<label for="judul">Judul</label>
			<input type="text" class="form-control" id="judul" name="judul" placeholder="" value="{{ old('judul', '') }}">
			@error('judul')
				<div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>

		<div class="form-group my-3">
			<label for="pertanyaan">Pertanyaan</label>
			<textarea class="form-control" id="pertanyaan" name="pertanyaan" placeholder="" >{{ old('pertanyaan', '') }}</textarea>
			@error('pertanyaan')
				<div class="alert alert-danger">{{ $message }}</div>
			@enderror
		</div>
		
		<div class="form-group mb-3">
			<label>Kategori</label>
			<br><br>
			@forelse ($kategori as $key => $value)
				<div class="form-check form-check-inline my-1 mx-n2">
					<input class="btn-check" name="{{$key+1}}" type="checkbox" value="{{$value->id}}" id="kategori{{$key}}">
					<label class="btn btn-outline-success btn-sm" for="kategori{{$key}}">
						{{$value->namakategori}}
					</label>
				</div>
			@empty
				<p>Kategori tidak tersedia</p>
			@endforelse
		</div>
		
		<div class="card-footer">
			<button type="submit" class="btn btn-primary">Post</button>
			<button type="button" class="btn btn-primary" onclick="document.location.href='{{ route('tanya.index') }}'">Cancel</button>
		</div>
	</form>
@endsection

@push('scripts')
	{{-- <script src="{{asset('tinymce/tinymce.min.js')}}" referrerpolicy="origin"></script> --}}
	<script src="https://cdn.tiny.cloud/1/t5n49s9tn5kfe9n6dl38k7x49n05u3ys2abkt3uo20s517hp/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
		tinymce.init({
		  selector: '#pertanyaan',
		  height: 500,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
			convert_urls: false,
			image_title: true,
			automatic_uploads: true,
			images_upload_url: '/upload-image',
			file_picker_types: 'image',
			file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                };
                input.click();
            }
		});
	  </script>
@endpush