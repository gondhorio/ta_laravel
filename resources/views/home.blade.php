@extends('adminlte.master') 

@section('content')
	
	<div class="container-fluid pt-4 px-4">
		<div class="row g-4">
			<div class="col-sm-12 col-md-6 col-xl-8">

					<div class="h-100 bg-light rounded p-4">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h6 class="mb-0">#Ask.com List Questions </h6>
							<a href="">Status</a>
						</div>
						
						<div class="d-flex align-items-center border-bottom py-3" style="cursor:pointer" onclick="document.location.href='/showask/1'">
							<img class="rounded-circle flex-shrink-0" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
							<div class="w-100 ms-3">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-0">Cannot type in TextInput: Always resets to defaultValue</h6>
									
									<button type="button" class="btn btn-sm btn-success rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
								</div>
								<small>Khomsun opened this issue on Apr 8, 2019 | <i class="fa fa-times"></i> 2 Answers</small>
							</div>
						</div>
						
						<div class="d-flex align-items-center border-bottom py-3" style="cursor:pointer" onclick="document.location.href='/showask/1'">
							<img class="rounded-circle flex-shrink-0" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
							<div class="w-100 ms-3">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-0">Cannot type in TextInput: Always resets to defaultValue</h6>
									<button type="button" class="btn btn-sm btn-success rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
								</div>
								<small>Khomsun opened this issue on Apr 8, 2019 | <i class="fa fa-times"></i> 2 Answers</small>
							</div>
						</div>
						<div class="d-flex align-items-center border-bottom py-3" style="cursor:pointer" onclick="document.location.href='/showask/1'">
							<img class="rounded-circle flex-shrink-0" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
							<div class="w-100 ms-3">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-0">Cannot type in TextInput: Always resets to defaultValue</h6>
									<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
								</div>
								<small>Khomsun opened this issue on Apr 8, 2019 | <i class="fa fa-times"></i> 2 Answers</small>
							</div>
						</div>
						
					</div>
				
			</div>
											
			
			@include('rightmenu')
			
		</div>
	</div>
			
	
		
@endsection

@push('scripts')
	<script>
	  
	</script>
@endpush