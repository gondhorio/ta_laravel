@extends('adminlte.master2') 

@section('content')

	<!-- Sign Up Start -->
        <div class="container-fluid">
            <div class="row h-100 align-items-center justify-content-center" style="min-height: 100vh;">
                <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
                    <div class="bg-light rounded">
                        <div class="d-flex align-items-center justify-content-between mb-3">
                            <a href="/" class="" style="display:flex">
								<button type="button" class="btn btn-square btn-outline-primary m-2"><i class="fa fa-home"></i></button>
								
                                <h3 class="text-primary" style="margin:0; padding-top:10px">ASK.COM</h3>
                            </a>
                            <h3>Sign Up</h3>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="text" class="form-control" id="floatingText" placeholder="jhondoe">
                            <label for="floatingText">Username</label>
                        </div>
                        <div class="form-floating mb-3">
                            <input type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
                            <label for="floatingInput">Email address</label>
                        </div>
                        <div class="form-floating mb-4">
                            <input type="password" class="form-control" id="floatingPassword" placeholder="Password">
                            <label for="floatingPassword">Password</label>
                        </div>
                        
                        <button type="submit" class="btn btn-primary py-3 w-100 mb-4">Sign Up</button>
                        <p class="text-center mb-0">Already have an Account? <a href="/signin">Sign In</a></p>
						<div align="center" style="padding:10px"><h6 style="cursor:pointer" onclick="document.location.href='/'"><i class="fa fa-chevron-circle-left"></i> Back</h6></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sign Up End -->
		
@endsection

@push('scripts')
	<script>
	  
	</script>
@endpush