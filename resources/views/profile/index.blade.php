@extends('adminlte.master')

@push('style')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
@endpush

@section('content')
<div class="container my-3">
    <div class="main-body">
    
          <!-- Breadcrumb -->
          {{-- <nav aria-label="breadcrumb" class="main-breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="index.html">Home</a></li>
              <li class="breadcrumb-item"><a href="javascript:void(0)">User</a></li>
              <li class="breadcrumb-item active" aria-current="page">User Profile</li>
            </ol>
          </nav> --}}
          <!-- /Breadcrumb -->
    
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    @if ($profile->avatar === null)
                        <div class="col-12 d-flex justify-content-center my-3">
                            <img src="{{ asset('images/default-profile.png') }}" alt="thumbnail_profile" class="rounded-circle" width="128px" height="128px">            
                        </div>
                    @else
                        <div class="col-12 d-flex justify-content-center my-3">
                            <img src="{{ asset('images/' . $profile->avatar) }}" alt="thumbnail_profile" class="rounded-circle" width="128px" height="128px">            
                        </div>
                    @endif
                    <div class="mt-3">
                      <h4>{{Auth::user()->name}} ({{$profile->umur}})</h4>
                      <p class="text-secondary mb-1">{{$profile->user->email}}</p>
                      <p class="text-muted font-size-sm">{{$profile->bio}}</p>
                    </div>
                  </div>
                </div>
              </div>
    
            </div>
            <div class="col-md-8">
              <div class="card mb-3">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{Auth::user()->name}}
                    </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{Auth::user()->email}}
                    </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Jenis Kelamin</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{$profile->jk}}
                    </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Alamat</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        {{$profile->alamat}}
                    </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Bio</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                      {{$profile->bio}}
                    </div>
                  </div>
                  <hr>

                  <div class="row">
                    <div class="col-sm-12">
                      <a class="btn btn-info " href="/profile/{{$profile->id}}/edit"><i class="bi bi-pencil-square"></i>Edit</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
@endsection