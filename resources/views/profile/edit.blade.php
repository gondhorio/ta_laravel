@extends('adminlte.master')

@push('style')
    <link rel="stylesheet" href="{{ asset('css/profile.css') }}">
@endpush

@section('content')
    <div class="container bg-soft my-2">
        <div class="row">
            @if ($profile->avatar === null)
                <div class="col-12 d-flex justify-content-center my-3">
                    <img src="{{ asset('images/default-profile.png') }}" alt="thumbnail_profile" class="rounded-circle" width="128px" height="128px">            
                </div>
            @else
                <div class="col-12 d-flex justify-content-center my-3">
                    <img src="{{ asset('images/' . $profile->avatar) }}" alt="thumbnail_profile" class="rounded-circle" width="128px" height="128px">            
                </div>
            @endif
            <div class="col-12">
                <form action="/profile/{{$profile->id}}" method="Post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="form-group mb-2">
                        <label for="user_name">Username</label>
                        <input type="text" class="form-control" id="user_name" value="{{ Auth::user()->name }}" disabled>
                    </div>

                    <div class="form-group mb-2">
                        <label for="surel">Email</label>
                        <input type="email" class="form-control" id="surel" value="{{ Auth::user()->email }}" disabled>
                    </div>

                    <div class="form-group mb-2">
                        <label for="age">Umur</label>
                        <input type="number" class="form-control" id="age" name="umur" value="{{ $profile->umur }}">
                    </div>
                    @error('umur')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group mb-2">
                        <label for="address">Alamat</label>
                        <textarea name="alamat" id="address" class="form-control">{{ $profile->alamat }}</textarea>
                    </div>
                    @error('alamat')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group mb-2">
                        <label for="gender">Jenis Kelamin</label>
                        @if ($profile->jk === "M")
                            <select class="form-control" name="jenis_kelamin" id="gender">
                                <option value="M" selected>Pria</option>
                                <option value="F">Wanita</option>
                                <option value="O">Lain-Lain</option>
                            </select>
                        @endif
                        @if ($profile->jk === "F")
                            <select class="form-control" name="jenis_kelamin" id="gender">
                                <option value="M">Pria</option>
                                <option value="F" selected>Wanita</option>
                                <option value="O">Lain-Lain</option>
                            </select>
                        @endif
                        @if ($profile->jk === "O")
                            <select class="form-control" name="jenis_kelamin" id="gender">
                                <option value="M">Pria</option>
                                <option value="F">Wanita</option>
                                <option value="O"selected>Lain-Lain</option>
                            </select>
                        @endif
                        @if ($profile->jk === null)
                            <select class="form-control" name="jenis_kelamin" id="gender">
                                <option value="M">Pria</option>
                                <option value="F">Wanita</option>
                                <option value="O">Lain-Lain</option>
                            </select>
                        @endif
                    </div>
                    <div class="form-group mb-2">
                      <label for="bio">Bio</label>
                      <textarea name="bio" id="bio" class="form-control" placeholder="Ceritakan siapa anda">{{ $profile->bio }}</textarea>
                    </div>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group mb-2">
                        <label for="foto">Change Photo Profile ?</label>
                        <input type="file" name="avatar" class="form-control">
                    </div>
                    @error('avatar')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group mt-2">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection