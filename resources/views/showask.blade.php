@extends('adminlte.master')

@section('content') 
	
	<div class="container-fluid pt-4 px-4">
		<div class="row g-4">
			<div class="col-sm-12 col-md-6 col-xl-8">
				
				<?php $statusclose = ""; ?>
				<?php $uid = ""; ?>
				
				@forelse ($ListPertanyaan as $key=>$value)
					
					<div class="d-flex w-100 align-items-center justify-content-between">
						<div>
							<h4>{{$value->judul}}</h4>				
							<small>{{$value->name}} opened this issue on {{ \App\Helper\Helper::setDateTimeVal($value->tgljam) }} | <i class="fa fa-times"></i> {{ $jmlJawab }} Answers</small>
							<div><small><b>Category :</b> {{ $kolomKategori }}</small></div>
							
						</div>			
						<div>
							<div align="center" style="cursor:pointer" onclick="document.location.href='/saveitem/{{ $value->id }}'">								
								<i class="fa fa-bookmark me-lg" style="font-size:22px; color:#F65200; padding:0px;"></i><br />
								<small style="padding:0px;">save</small>
							</div>
							
							<hr />
						@if($value->isclosed == '1')
							<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
						@else
							<button type="button" class="btn btn-sm btn-success rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
						@endif
						</div>
					</div>							
					<hr />
					
					<?php $statusclose = $value->isclosed;?>
					<?php $uid = $value->user_id;?>
					
					<div class="alert alert-info alert-dismissible fade show" role="alert">
						<div>
							<img class="rounded-circle me-lg-2" src="{{ asset('/images/'.$value->avatar) }}" alt="" style="width: 40px; height: 40px;">
							<small>{{$value->name}} question on {{ \App\Helper\Helper::setDateTimeVal($value->tgljam) }}</small>
							<!--<button type="button" class="btn-close" </button>
							-->
							<div style="position:absolute; right:5px; top:5px;">
								<i class="fas fa-pencil-alt" style="font-size:18px; cursor:pointer" onclick="document.location.href='/tanya/{{ $value->id }}/edit'"></i>								
							</div>
							
						</div>
						<hr />
						<div class="alert alert-light" role="alert">							
							{!! $value->pertanyaan !!}
						</div>																	
					</div>
					
					<hr />
					
					@if($value->isclosed == '1')
						<div class="d-flex w-100 align-items-center justify-content-between">
							<h5>{{ $jmlJawab }} Answers</h5>																
						</div>
						
					@else							
																
						<div class="d-flex w-100 align-items-center justify-content-between">
							<h5>{{ $jmlJawab }} Answers</h5>	
							
							@if($value->user_id == Auth::id())
							<div></div>
							@else
							<button id="btnAddJawab" type="button" class="btn btn-primary m-2" onclick="
								$('#btnAddJawab').toggle()
								$('#divJawaban').toggle('medium')								
							">Add Answer</button>	
							@endif
											
						</div>
						
						<div id="divJawaban" style="display:none">
						<form action="/showask/sv" method="post">
						@csrf
							<div class="d-flex w-100 align-items-center justify-content-between">
								<h6>Your Answer</h6>
								<div>
									<button type="submit" class="btn btn-primary m-2">Post Answer</button>
									<button type="button" class="btn btn-secondary m-2" onclick="										
										tinyMCE.activeEditor.setContent('');
										$('#btnAddJawab').toggle()
										$('#divJawaban').toggle('medium')
									">Cancel</button>
								</div>
							</div>
							<input name="tanyaid" type="hidden" value="{{$value->id}}" />
							<textarea class="form-control" placeholder="Leave a comment here" id="txtjawaban" name="txtjawaban" style="height: 150px;"></textarea>
						</form>	
						</div>
					
					@endif
								
					
					
				@empty
					<div class="d-flex align-items-center border-bottom py-3">
					<div align="center" style="width:100%">Data tidak ditemukan</div>
					</div>
				@endforelse  

				<hr />
				
				@forelse ($ListJawaban as $keyXX=>$valueXX)									
					
					<div class="alert alert-secondary">
						<div>
							<img class="rounded-circle me-lg-2" src="{{ asset('/images/'.$valueXX->avatar) }}" alt="" style="width: 40px; height: 40px;">
							<small>{{$valueXX->name}} comment on {{ \App\Helper\Helper::setDateTimeVal($valueXX->tgljam) }}</small>							 
						</div>
						<hr />
						<div class="alert alert-light" role="alert">
							{!! $valueXX->jawaban !!}
						</div>	
						
						<div class="d-flex w-100 align-items-center justify-content-between">
							
							<?php if (abs($statusclose) === 0 && $uid == Auth::id()){ ?>
								<form name="frmcls_{{ $valueXX->id }}" action="/showask/c" method="post">
								@csrf															
									<input name="askid" type="hidden" value="{{$valueXX->tanya_id}}" />	
									<input name="jid" type="hidden" value="{{$valueXX->id}}" />
								
								<button type="submit" class="btn btn-outline-success m-2"><i class="fa fa-check-circle"></i> Set Close with this Answer</button>
								</form>
							<?php }else{ ?>
								<div>
								@if($valueXX->issolutions == '1')
								<small style="color:#0000FF">Correct Answer <i class="fa fa-thumbs-up"></i></small>
								@endif
								</div>
							<?php }?>
							
							<div class="btn-group" role="group">
							
							<form name="frm_{{ $valueXX->id }}" action="/showask/h" method="post">
							@csrf								
								<input name="uid" type="hidden" value="{{$valueXX->user_id}}" />
								<input name="askid" type="hidden" value="{{$valueXX->tanya_id}}" />								
								<input name="rid" type="hidden" value="{{$valueXX->id}}" />															
								<input name="st" id="st_{{ $valueXX->id }}" type="hidden" />	
								
								<button type="submit" class="btn btn-outline-primary m-2" onclick="getElementById('st_{{ $valueXX->id }}').value=1">
									<i class="fa fa-thumbs-up"></i> Helpfull <span>({{ $kolomHelpfull[$valueXX->id.$valueXX->tanya_id.$valueXX->user_id] }})</span>
								</button>
								
								<button type="submit" value="0" class="btn btn-outline-primary m-2" onclick="getElementById('st_{{ $valueXX->id }}').value=0">
									<i class="fa fa-thumbs-down"></i> Not Helpfull <span>({{ $kolomNotHelpfull[$valueXX->id.$valueXX->tanya_id.$valueXX->user_id] }})</span>
								</button>																			
							</form>	
																					
													
							</div>
						</div>								
					</div>
					
				@empty
					
				@endforelse  																				
				
				
			</div>
												
			
			@include('rightmenu')
			
		</div>
	</div>
			
	
		
@endsection

@push('scripts')
	<script>
	  function setClose(askid){
	  		document.getElementById('spinner').classList.add("show");
			$.ajax({
				type: "GET",
				url: "/showask/c/"+askid,
				data: 'st=',
				cache: false,
				async: false,
				success: function(hsl){ alert(hsl);
					document.getElementById('spinner').classList.remove("show");
					//document.location.reload();
				},
				error: function(err){
					document.getElementById('spinner').classList.remove("show");
					alert('err = '+err);
				}
			});
	  }
	 
	</script>
	<script src="https://cdn.tiny.cloud/1/t5n49s9tn5kfe9n6dl38k7x49n05u3ys2abkt3uo20s517hp/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
		tinymce.init({
		  selector: '#txtjawaban',
		  height: 500,
            setup: function (editor) {
                editor.on('init change', function () {
                    editor.save();
                });
            },
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
			convert_urls: false,
			image_title: true,
			automatic_uploads: true,
			images_upload_url: '/upload-image',
			file_picker_types: 'image',
			file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];

                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                    };
                };
                input.click();
            }
		});
	  </script>
@endpush