@extends('adminlte.master2') 

@section('content')
	<!-- Navbar Start -->
	@include('adminlte.partial.navbar2')
	<!-- Navbar End -->
	
	<main class="main" id="top">
     
		<section class="pt-7">
			<div class="container">
			  <div class="row align-items-center">
				<div class="col-md-6 text-md-start text-center py-6">
				  <h1 class="mb-4 fs-9 fw-bold">Team 5 Tugas Akhir Laravel</h1>
				  <p class="mb-6 lead text-secondary">Website ini adalah web yang digunakan 
				  <br class="d-none d-xl-block" />sebagai media tanya jawab yang merupakan 
				  <br class="d-none d-xl-block" />tugas akhir dari bootcamp laravel di sanbercode</p>
				  <div class="text-center text-md-start"><a class="btn btn-warning me-3 btn-lg" href="/login" role="button">Get started</a></div>
				</div>
				<div class="col-md-6 text-end" style="padding:50px"><img class="pt-7 pt-md-0 img-fluid" src="{{ asset('/adminlte/img/validation.png') }}" alt="" /></div>
			  </div>
			</div>
		  </section>
	</main>
		
@endsection

@push('scripts')
	<script>
	  
	</script>
@endpush