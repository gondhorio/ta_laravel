@extends('layouts.app')

@section('content')
<!-- Sign In Start -->
<div class="container-fluid">
    <div class="row h-100 align-items-center justify-content-center" style="min-height: 100vh;">
        <div class="col-12 col-sm-8 col-md-6 col-lg-5 col-xl-4">
            <div class="bg-light rounded p-5">
                <div class="d-flex align-items-center justify-content-between mb-3">
                    <a href="/" class="" style="display:flex">
                        <button type="button" class="btn btn-square btn-outline-primary m-2"><i class="fa fa-home"></i></button>
                        
                        <h3 class="text-primary" style="margin:0; padding-top:10px">ASK.COM</h3>
                    </a>
                    <h3>Sign In</h3>
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-floating mb-3">
                        <input type="email" id="floatingInput" placeholder="name@example.com" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">
                        <label for="floatingInput">Email address</label>
                        @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    <div class="form-floating mb-4">
                        <input type="password" id="floatingPassword" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                        <label for="floatingPassword">Password</label>
                        @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>
                    
                    <button type="submit" class="btn btn-primary py-3 w-100 mb-4">Sign In</button>
                </form>
                <p class="text-center mb-0">Don't have an Account? <a href="/register">Sign Up</a></p>
                <div align="center" style="padding:10px"><h6 style="cursor:pointer" onclick="document.location.href='/'"><i class="fa fa-chevron-circle-left"></i> Back</h6></div>
            </div>
        </div>
    </div>
</div>
<!-- Sign In End -->

@endsection
