@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">
		<div class="w-100 align-items-center justify-content-between">
			<center>
				<h1>My Activity</h1>
			</center>
		</div>
		<hr/>
			@forelse (App\Aktifitas::where('user_id', Auth::id())->get() as $key=>$value)
			<div class="row g-4 mb-3">
				<div class="col-12">
					<h4 class="d-inline">{{$value->nama_aktifitas}}</h4><br>			
					<small>
						{{App\User::find(Auth::id())->name}} activity on <?=$formatDate = date('M d, Y', strtotime($value->tgljam))?>
						
						<p>{{$value->deskripsi}}</p>
					</small>
											
					<hr />
																		
				</div>	
			</div>
			@empty
				<p>No Data</p>
			@endforelse              
	</div>
	
@endsection

@push('scripts')
	
@endpush