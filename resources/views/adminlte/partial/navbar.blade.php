<!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0"> 
                <a href="#" class="navbar-brand d-flex d-lg-none me-4">
                    <h2 class="text-primary mb-0"><i class="fa fa-hashtag"></i></h2>
                </a>
                <a href="#" class="sidebar-toggler flex-shrink-0">
                    <i class="fa fa-bars"></i>
                </a>
                <form action="/admin/search" method="post" style="width:100%">
				@csrf
                    <input class="form-control border-0" type="search" name="txtCari" value="{{ old('txtCari', '') }}" style="width:100%" placeholder="What is your question?">
                </form>
                <div class="navbar-nav align-items-center ms-auto">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <i class="fa fa-bookmark me-lg-2"></i>
                            <span class="d-none d-lg-inline-flex">Saving Items</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                            @forelse (App\Saveitems::where('user_id', Auth::id())->orderBy('tanya_id', 'desc')->get() as $key => $item)
                                @if ($key < 5)    
                                    <a href="/showask/{{$item->tanya_id}}" class="dropdown-item">
                                        <div class="d-flex align-items-center">                                   
                                            <div class="ms-2">
                                                <small>{{App\Tanya::find($item->tanya_id)->judul}}</small>                                        
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            @empty
                                <p>No Saved data</p>
                            @endforelse
                            <hr class="dropdown-divider">
							<div class="dropdown-item text-center">
                            <a href="/saveitem" class="btn btn-outline-info m-2">See all saving items</a>
							</div>
                         
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            <i class="fa fa-history me-lg-2"></i>
                            <span class="d-none d-lg-inline-flex">Activity</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                            @forelse (App\Aktifitas::where('user_id', Auth::id())->orderBy('tgljam', 'desc')->get() as $key => $item)
                                
                                @if ($key < 5)
                                    <a href="#" class="dropdown-item">
                                        <small>{{$item->nama_aktifitas}}</small>
                                        <div style="font-size:10px">
                                            {{time_elapsed_string($item->tgljam)}}
                                        
                                        </div>
                                    </a>
                                    <hr class="dropdown-divider">
                                @endif
                            @empty
                                <p>No Activity</p>
                            @endforelse
							<div class="dropdown-item text-center">
                            <a href="/myactivity" class="btn btn-outline-info m-2">See all activity</a>
							</div>
							
                        </div>
                    </div>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">
                            @if (!Auth::user()->profile->avatar == null)
                                <img src="{{ asset('images/' . Auth::user()->profile->avatar) }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">
                            @else
                                <img src="{{ asset('images/default-profile.png') }}" alt="thumbnail_profile" class="rounded-circle" width="40px" height="40px">            
                            @endif
                            <span class="d-none d-lg-inline-flex">{{Auth::user()->name}}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end bg-light border-0 rounded-0 rounded-bottom m-0">
                            <a href="/profile" class="dropdown-item">My Profile</a>                            
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Navbar End -->