<!-- Navbar Start -->
            <nav class="navbar navbar-expand bg-light navbar-light sticky-top px-4 py-0"> 
                <a href="/" class="navbar-brand mx-4 mb-3">
                    <h3 class="text-primary" style="margin:0; padding:0;"><i class="fa fa-hashtag me-2"></i>ASK.COM</h3>
					<span style="font-size:12px; margin:0; padding:0; position:absolute; top:1; padding-left:30px">Forum Tanya Jawab</span>
                </a>
                              
                <div class="navbar-nav align-items-center ms-auto">                  
                     <div class="d-flex ms-lg-4"><a class="btn btn-outline-primary" href="/login">Sign In</a><a class="btn btn-primary ms-3" href="/register">Sign Up</a></div>
                </div>
            </nav>
            <!-- Navbar End -->