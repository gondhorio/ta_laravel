@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">
		<div class="w-100 align-items-center justify-content-between">
			<center>
				<h1>Saved Items</h1>
			</center>
		</div>
		<hr/>
			@forelse ($tanya as $key=>$value)
			<div class="row g-4 mb-3">
				<div class="col-sm-12 col-md-6 col-xl-12">
					
					<div class="d-flex w-100 align-items-center justify-content-between">
						<div>
							<h4 class="d-inline">{{$value->judul}}</h4>	<br>			
							<small>
								{{App\User::find($value->user_id)->name}} opened this issue on <?=$formatDate = date('M d, Y', strtotime($value->tgljam))?> | 
								@if($value->isclosed == 0)				
									<i class="fa fa-times"></i>
								@else 
									<i class="fa fa-check"></i>
								@endif
								 {{count(App\Tanya::find($value->id)->jawab)}} Answers
							</small>
							<div>
								<small><b>Category :</b> 
									@forelse ($kategori as $item)
										@if ($item->tanya_id == $value->id)
											<div class="btn btn-secondary btn-sm rounded-pill my-2" style="cursor:none; font-size:10px">
												{{$item->namakategori}}
											</div>
										@endif
									@empty
										No kategori
									@endforelse
								</small>
							</div>
							<div class="d-flex my-2">
								<a href="/tanya/{{$value->id}}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Detail</a>
								<a class="btn btn-primary btn-sm mx-2" href="/showask/{{$value->id}}" role="button"><i class="fas fa-location-arrow"></i> Go To Post</a>
							</div>
						</div>	
		
						@if($value->isclosed == 0)				
							<button type="button" class="btn btn-sm btn-success rounded-pill m-2" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
						@else 
							<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
						@endif
					</div>								
					<hr />
																		
				</div>	
			</div>
			@empty
				<p>No Data</p>
			@endforelse              
	</div>
	
@endsection

@push('scripts')
	
@endpush