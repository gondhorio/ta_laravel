<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <style>
        .bg-soft{
            background-color: lightskyblue;
        }

        .opacity-0 {
            opacity: 0;
        }

        .opacity-1 {
            opacity: 0.2;
        }

        .opacity-2 {
            opacity: 0.4;
        }

        .opacity-3 {
            opacity: 0.6;
        }

        .opacity-4 {
            opacity: 0.8;
        }
        .opacity-5 {
            opacity: 1;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
    <section>
        <div class="container">
            {{-- Judul Section --}}
            <div class="row">
                <div class="col-12 text-center">
                    <h1>Report Tanya dan Jawab</h1><hr>
                </div>
            </div>   
        </div>
    </section>

    <section>
        {{-- Head Section --}}
        <div class="container-fluid">
            @forelse ($tanya as $nanya)
            <div class="row mb-3">
                <div class="col-md-12 mb-2">
                    <div class="d-inline">
                        Tanggal Posting :
                    </div>
                    <div class="d-inline">
                        <?php
                            date_default_timezone_set("Asia/Jakarta");
                            echo date("Y-m-d h:i:s a");  
                        ?>
                    </div>
                    
                    <div class="row mr-5 mt-1">
                        <div class="col-md-12">
                            <div class="card bg-soft opacity-4">
                                <div class="card-body">
                                        <h5 class="card-title mb-3">{{$nanya->judul}}</h5>
                                        <h6 class="card-subtitle mb-2 text-muted">
                                            @forelse ($kategoris as $kategori)
                                                @if ($kategori->tanya_id == $nanya->id)
                                                    <div class="d-inline">
                                                        <span class="badge badge-pill badge-danger">{{$kategori->namakategori}}</span>
                                                    </div>
                                                @endif
                                            @empty
                                                <h1>Empty Kategori</h1>
                                            @endforelse
                                        </h6>
                                        <p class="card-text">{!!$nanya->pertanyaan!!}</p>
                                </div>
                            </div>

                            <section>
                                {{-- Detail Section --}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr> 
                                            {{-- <p class="text-center"><span class="badge badge-dark">{{count($jawab->tanya->where('tanya_id', $nanya->id)->get())}}</span> Jawaban</p>      --}}
                                            <p class="text-center"><span class="badge badge-dark">{{count($jawab_all->where('tanya_id', $nanya->id))}}</span> Jawaban</p>
                                            <hr>
                                    </div>
                                </div>
                                @foreach ($jawab_all as $answer => $value)
                                    @if ($nanya->id == $value->tanya_id)
                                        <div class="row mr-3 mb-3">
                                            <div class="col-md-12">
                                                <div class="card bg-soft opacity-4">
                                                    <div class="card-body">
                                                    <h5 class="card-title mb-3"><b>{{$jawab->user->find($value->user_id)->name}}</b> comment on {{$formatDate = date('M d, Y', strtotime($value->tgljam))}}</h5>
                                                    @if ($value->issolutions != 0)
                                                        <span class="badge badge-pill badge-success">Solution</span>
                                                    @endif
                                                    <p class="card-text">{{$value->jawaban}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Else Masih ada bug --}}
                                    {{-- @else
                                        <div class="row mr-5 mt-3">
                                            <div class="col-md-12">
                                                <div class="card bg-soft opacity-4">
                                                    <div class="card-body">
                                                    <h5 class="card-title mb-3">Tidak ada komentar</h5>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                    @endif    
                                @endforeach
                            </section>
                        </div>
                    </div>  
                </div>
            </div>
            <div class="page-break"></div>
            @empty
                <h1>Data Pertanyaan tidak ada</h1>
            @endforelse
        </div>
    </section>
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>