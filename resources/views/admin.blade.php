@extends('adminlte.master') 

@section('content')
	
	<div class="container-fluid pt-4 px-4">
		<div class="row g-4">
			<div class="col-sm-12 col-md-6 col-xl-8">

					<div class="h-100 bg-light rounded p-4">
						<div class="d-flex align-items-center justify-content-between mb-2">
							<h6 class="mb-0">List Pertanyaan 
							@if(isset($cat))
								{{ $cat }}
							@endif 
 							</h6>

							<a href="">Status</a>							
						</div>										
						
						@forelse ($ListPertanyaan as $key=>$value)
                    		<div class="d-flex align-items-center border-bottom py-3" style="cursor:pointer" onclick="document.location.href='/showask/{{ $value->id }}'">
								<img class="rounded-circle flex-shrink-0" src="{{ asset('/adminlte/img/user.jpg') }}" alt="" style="width: 40px; height: 40px;">
								<div class="w-100 ms-3">
									<div class="d-flex w-100 justify-content-between">
										<h6 class="mb-0">{{$value->judul}}</h6>
										
										@if($value->isclosed == '1')
											<button type="button" class="btn btn-sm btn-danger rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-check-circle"></i> Closed</button>
										@else
											<button type="button" class="btn btn-sm btn-success rounded-pill" style="cursor:none; font-size:10px"><i class="fa fa-times"></i> Opened</button>
										@endif 
							
										
									</div>
									
									<small> Category : 
									@forelse ( $kolomKategori[$value->id] as $keyCat=>$valCat )
										
										<span class="btn-primary" style="padding:5px; padding-top:1px; padding-bottom:1px; font-size:10px; margin:3px; border-radius:5px; color:#FFFFFF">{{ $valCat->namakategori }} </span>
									
									@empty
									
									@endforelse
									</small>
									
									<br />
									<small>{{$value->name}} opened this issue on  
									{{ \App\Helper\Helper::setDateTimeVal($value->tgljam) }} | <i class="fa fa-times"></i> {{ $kolomJml[$value->id] }} Answers</small>
									<br />
									<a style="color:#0000FF; font-size:12px">View Detail</a>
								</div>
							</div>
						@empty
							<div class="d-flex align-items-center border-bottom py-3">
							<div align="center" style="width:100%">Data tidak ditemukan</div>
							</div>
						@endforelse       
						
					</div>
				
			</div>
												
			
			@include('rightmenu')
			
		</div>
	</div>
			
	
		
@endsection

@push('scripts')
	<script>
	  
	</script>
@endpush