<div class="col-sm-12 col-md-6 col-xl-4"> 
	<div class="h-100 bg-light rounded p-4">
		<div class="d-flex align-items-center justify-content-between mb-4">
			<h6 class="mb-0">Browse By Category</h6>			
		</div>
		<div class="d-flex mb-2">
			<input class="form-control bg-transparent" type="text" onkeyup="FilterItem(this.value)" placeholder="Enter category">			
		</div>
		<div id="listCategory">
			<div class="d-flex align-items-center border-bottom py-2">						
				<div class="w-100 ms-3">
					<div class="d-flex w-100 align-items-center justify-content-between">
						<span>category not found</span>								
					</div>
				</div>
			</div>
		</div>
		
		
	</div>
</div>

<script>
var dataCount = 0;
var datas = [];
<?php for($i=0; $i<count($ListKategori); $i++){?>
datas.push([ {{ $ListKategori[$i]->id }}, '{{ $ListKategori[$i]->namakategori }}' ]);
<?php }?>

ShowCategory(datas);

function ShowCategory(itemsCategory){
	var listCategory = document.getElementById("listCategory");
	var dataKategori = '';
	for (var i=0; i<itemsCategory.length; i++){
		var kat = itemsCategory[i];
		
		dataKategori += `<div class="d-flex align-items-center border-bottom py-2" style="cursor:pointer" onclick="document.location.href='/cat/${kat[0]}'">						
				<div class="w-100 ms-3">
					<div class="d-flex w-100 align-items-center justify-content-between">
						<span>${kat[1]}</span>								
					</div>
				</div>
			</div>`;		
	}
	
	listCategory.innerHTML = dataKategori;
}

function FilterItem(term){	
	var hasilFilter = [];
	for (var i=0; i<datas.length; i++){
		var hasil = datas[i].filter(item => item.toString().toLowerCase().indexOf(term) > -1);		
		if (hasil != '')		
			hasilFilter.push(datas[i]);
	}	
	ShowCategory(hasilFilter);
}
</script>