@extends('adminlte.master')

@section('content') 
	<div align="center">		
		<h5>Tambah Data</h5>
		<hr />
	</div>
	
	<form action="{{ route('kategori.index') }}" method="post">
	@csrf
	<div class="card-body">
		<div class="form-group">
		<input type="hidden" name="user_id" value="{{Auth::id()}}">
		<label for="exampleInputEmail1">Nama Kategori</label>
		<input type="text" class="form-control" id="namakategori" name="namakategori" placeholder="" value="{{ old('namakategori', '') }}">
		@error('namakategori')
			<div class="alert alert-danger">{{ $message }}</div>
		@enderror
		</div>
		
		
		</div>
		
		<div class="card-footer">
			<button type="submit" class="btn btn-primary">Save</button>
			<button type="button" class="btn btn-primary" onclick="document.location.href='{{ route('kategori.index') }}'">Cancel</button>
		</div>
	</form>
@endsection

@push('scripts')
	
@endpush