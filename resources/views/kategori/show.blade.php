@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">		
		<div class="d-flex w-100 align-items-center justify-content-between">
			<button type="button" class="btn btn-primary" onclick="document.location.href='{{ route('kategori.index') }}'"><i class="fa fa-arrow-circle-left"></i> Back</button>
			<h5>View Data</h5>
		</div>
		<hr/>
		
		<h5>Nama Kategori : {{$kategori->namakategori}}</h5>
	</div>
	
@endsection

@push('scripts')
	
@endpush