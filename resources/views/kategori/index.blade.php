@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">
		<div class="d-flex w-100 align-items-center justify-content-between">
			<a href="{{ route('kategori.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
			<h5>List Data Kategori</h5>
		</div>
		<hr/>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th style="width: 10px">#</th>
					<th>Nama Kategori</th>					
					<th style="width: 40px">Action</th>
				</tr>
			</thead>
			<tbody>
                @forelse ($data as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->namakategori}}</td>                       
                        <td style="display:flex;">                          
							<button title="Show" type="button" style="margin:2px" class="btn btn-info" 
							onclick="document.location.href='{{ route('kategori.show', ['kategori' => $value->id] ) }}'"><i class="fa fa-search"></i></button>
							<button title="Edit" type="button" style="margin:2px" class="btn btn-primary" 
							onclick="document.location.href='{{ route('kategori.edit', ['kategori' => $value->id] ) }}'"><i class="fas fa-pencil-alt"></i></button>
							
                            <form action="{{ route('kategori.destroy', ['kategori' => $value->id] ) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                
								<button type="submit" title="Delete" style="margin:2px" class="btn btn-danger"><i class="fa fa-times"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td  colspan="3"><div align="center">No data</div></td>
                    </tr>  
                @endforelse              
            </tbody>
		</table>
	</div>
	
@endsection

@push('scripts')
	
@endpush