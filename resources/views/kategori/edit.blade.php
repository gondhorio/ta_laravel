@extends('adminlte.master')

@section('content') 
	
	<div class="card-body">		
		<div>
			<h2>Edit Data</h2>
			<form action="{{ route('kategori.update', ['kategori' => $kategori->id] ) }}" method="POST">
				@csrf
				@method('PUT')
				<div class="form-group">
					<label for="title">Nama Kategori</label>
					<input type="hidden" name="user_id" value="{{Auth::id()}}">
					<input type="text" class="form-control" name="namakategori" value="{{$kategori->namakategori}}" id="namakategori" placeholder="Masukkan Nama Kategori">
					@error('namakategori')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				
				<hr />
				<button type="submit" class="btn btn-primary">Edit</button>
				<button type="button" class="btn btn-info" onclick="document.location.href='{{ route('kategori.index') }}'">Cancel</button>
			</form>
		</div>
	</div>
	
@endsection

@push('scripts')
	
@endpush