<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropColumnKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kategori', function (Blueprint $table) {
			if (Schema::hasColumn('kategori','user_id')) {
              	$table->dropForeign(['user_id']);
				$table->dropColumn('user_id');			
			}
			if (Schema::hasColumn('kategori','isdel')) {              	
				$table->dropColumn('isdel');
			}
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
