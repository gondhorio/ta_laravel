<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanyaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanya', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tgljam', 14);
			$table->string('judul', 200);
			$table->longText('pertanyaan');
			$table->char('isclosed', 1)->default('0');
			$table->unsignedBigInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanya');
    }
}
