<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyForeignKategoriPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('kategori_pertanyaan', function (Blueprint $table) {
            $table->dropForeign('kategori_pertanyaan_tanya_id_foreign');
            $table->dropForeign('kategori_pertanyaan_kategori_id_foreign');

            $table->foreign('tanya_id')->references('id')->on('tanya')->onUpdate('cascade') ->onDelete('cascade');
            
			$table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('cascade') ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
