<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRespondTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respond', function (Blueprint $table) {         
            $table->bigIncrements('id');
			$table->tinyInteger('helpfull')->default(0);
			$table->unsignedBigInteger('jawab_user_id');
			$table->foreign('jawab_user_id')->references('user_id')->on('jawab');
			$table->unsignedBigInteger('jawab_tanya_id');
			$table->foreign('jawab_tanya_id')->references('tanya_id')->on('jawab');
			$table->unsignedBigInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respond');
    }
}
