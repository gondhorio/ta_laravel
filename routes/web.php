<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { 
    return view('index');
});

/*
Route::get('/signup', function () {
    return view('signup');
});

Route::get('/signin', function () {
    return view('signin');
});
*/ 

Route::get('/admin', 'AdminController@index');
Route::post('/admin/search', 'AdminController@searchByName');
Route::get('/cat/{cat_id}', 'AdminController@searchKategori');

Route::get('/showask/{ask_id}', 'ShowAskController@index');
Route::post('/showask/c/', 'ShowAskController@setclose');
Route::post('/showask/h', 'ShowAskController@helpfull');
Route::post('/showask/sv', 'ShowAskController@setjawaban');

Route::resource('kategori','KategoriController'); 

Auth::routes(); 

Route::get('/home', 'AdminController@index')->name('home');

Route::resource('/profile', 'ProfileController')->only([
    'index', 'update', 'edit'
]);

Route::resource('tanya','TanyaController'); 

Route::post('/upload-image', 'TanyaController@upload');

Route::get('/print-pdf', 'ReportController@show');

Route::get('/saveitem/{tanya_id}', 'SaveitemsController@saveitem');

Route::get('/saveitem', 'SaveitemsController@index');

Route::get('/myactivity', 'AktifitasController@index')->name('aktifitas.index');