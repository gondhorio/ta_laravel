<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jawab extends Model
{
	public $timestamps = false;
    protected $table = "jawab";
    protected $fillable = ["user_id", "tanya_id", "tgljam", "jawaban", "issolutions"];
	
	public function user()
  	{
   		return $this->belongsTo('App\User');
  	}  
  	public function tanya()
  	{
   		return $this->belongsTo('App\Tanya');
  	}
	
	public function respond(){
		return $this->belongsTo('App\Respond');
	}
}
