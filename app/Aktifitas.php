<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aktifitas extends Model
{
    protected $table = "aktifitas";
    protected $fillable = ["tgljam", "nama_aktifitas", "deskripsi", "user_id"];
    public $timestamps = false;
	
	public function user(){
        return $this->belongsTo('App\User');
    }
}
