<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
	public $timestamps = false;
    protected $table = "kategori"; 
    protected $fillable = ["namakategori", "tglbuat"];

    public function kategori_pertanyaan(){
        return $this->hasMany('App\Kategori_pertanyaan');
    }
}
