<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saveitems extends Model 
{
    protected $table = "saveitems";
    protected $fillable = ["tanya_id", "user_id"];
	public $timestamps = false;
	
	public function user()
  	{
   		return $this->belongsTo('App\User');
  	}  
  	public function tanya()
  	{	
   		return $this->belongsTo('App\Tanya');
  	}
}
