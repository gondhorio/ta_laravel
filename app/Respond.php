<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respond extends Model
{
	public $timestamps = false;
    protected $table = "respond";
    protected $fillable = ["helpfull", "jawab_user_id", "jawab_tanya_id", "user_id"];
	
	public function user(){
		return $this->hasOne('App\User');
	}
	
	public function jawab(){
		return $this->hasOne('App\Jawab');
	}
}
