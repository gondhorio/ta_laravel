<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['umur', 'avatar', 'alamat', 'jk', 'bio', 'user_id'];
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
