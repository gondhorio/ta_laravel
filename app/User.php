<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne('App\Profile');
    }
	
	public function aktifitas(){
        return $this->belongsTo('App\Aktifitas');
    }
	
	public function respond(){
		return $this->belongsTo('App\Respond');
	}
	
	public function tanya()
	{
	   return $this->belongsTo('App\Tanya');
	}  
	
    public function jawab()
    {
        return $this->hasMany('App\Jawab');
    }
}
