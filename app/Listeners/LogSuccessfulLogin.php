<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Aktifitas;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        
        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Login";
        $aktifitas->deskripsi = $event->user->name;
        $aktifitas->user_id = $event->user->id;

        $aktifitas->save();
    }
}
