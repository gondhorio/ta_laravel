<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tanya extends Model
{  
    protected $table = 'tanya';
	protected $fillable = ["user_id", "tgljam", "judul", "pertanyaan", "isclosed"];
    protected $guarded = ['updated_at, created_at'];
	 
	public function user()
  	{
    	return $this->hasMany('App\User');
  	}

	  public function jawab()
  	{
    	return $this->hasMany('App\Jawab');
  	}
}
