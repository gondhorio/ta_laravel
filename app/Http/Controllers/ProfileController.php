<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;
// use File;
use App\Profile;
use App\Aktifitas;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }

    public function edit(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request){
        // dd($request);
        $request->validate([
            'umur' => 'required',
            'alamat' => 'required',
            'bio' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $profile = Profile::find($id);

        if ($request->has('avatar')) {
            // Post->delete();
            $path = "images/";
            File::delete($path . $profile->avatar);
            $imageName = time().'.'.$request->avatar->extension();  

            $request->avatar->move(public_path('images'), $imageName);

            $profile->avatar = $imageName;
        }
 
        $profile->umur = $request->umur;
        
        $profile->alamat = $request->alamat;
        $profile->jk = $request->jenis_kelamin;
        $profile->bio = $request->bio;
        $profile->user_id = Auth::id();
 
        $profile->save();

        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Edit Profile";
        $aktifitas->deskripsi = Auth::user()->name;
        $aktifitas->user_id = Auth::id();
        $aktifitas->save();

        // menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data Berhasil diperbaharui');

        return redirect('/profile');
    }
}
