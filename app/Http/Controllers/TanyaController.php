<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tanya;
use App\Kategori;
use App\Kategori_pertanyaan;
use App\Aktifitas;
use Illuminate\Support\Facades\Auth;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class TanyaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $tanya = Tanya::where('user_id', Auth::id())->orderBy('tgljam', 'desc')->get();
        $kategori = DB::table('kategori_pertanyaan')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->join('tanya', 'tanya_id', '=', 'tanya.id')
            ->select('kategori_pertanyaan.*', 'kategori.*', 'tanya.*')
            ->get();

        return view('tanya.index', compact('tanya', 'kategori'));
    }

    public function create()
    {
        $kategori = Kategori::all();
        return view('tanya.create', compact('kategori'));
    }

    public function upload(Request $request)
    {
        $fileName=time().$request->file('file')->getClientOriginalName();
        $path=$request->file('file')->storeAs('uploads', $fileName, 'public');
        return response()->json(['location'=>"/storage/$path"]); ;
    }


    public function store(Request $request){
        $this->validate($request,[
            'judul' => 'required',
            'pertanyaan' => 'required',
        ]);
        // dd($request);
        $tanya = new Tanya;
        $tanya->tgljam = date('YmdHis');
        $tanya->judul = $request->judul;
        $tanya->pertanyaan = $request->pertanyaan;
        $tanya->isClosed = 0;
        $tanya->user_id = Auth::id();
        
        $tanya->save();

        foreach ($request->all() as $key => $value) {
            if($key != "_token" && $key != "judul" && $key != "pertanyaan"){
                // dd($value);
                $kategoriPertanyaan = new Kategori_pertanyaan;
 
                $kategoriPertanyaan->tanya_id = $tanya->id;
                $kategoriPertanyaan->kategori_id = $value;
                
                $kategoriPertanyaan->save();
            }
        }

        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Post Pertanyaan";
        $aktifitas->deskripsi = $request->judul;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();

        return redirect('tanya');
    }

    public function show($id)
    {
        $tanya = Tanya::find($id);
        $kategori = DB::table('kategori_pertanyaan')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->join('tanya', 'tanya_id', '=', 'tanya.id')
            ->select('kategori_pertanyaan.*', 'kategori.*', 'tanya.*')
            ->get();
        return view('tanya.show', compact('tanya', 'kategori'));
    }


    public function edit($id){
        $tanya = Tanya::find($id);
        $kategori = Kategori::all();
        $kategoriPertanyaan = Kategori_Pertanyaan::where('tanya_id', $id)->get();
        return view('tanya.edit', compact('tanya', 'kategori', 'kategoriPertanyaan'));
    }

    public function update($id, Request $request){
        // dd($request);
        $this->validate($request,[
            'judul' => 'required',
            'pertanyaan' => 'required',
        ]);

        $tanya = Tanya::find($id);

        $tanya->judul = $request->judul;
        $tanya->pertanyaan = $request->pertanyaan;
        
        $tanya->save();

        $currentKategoriPertanyaan = Kategori_Pertanyaan::where('tanya_id', $id)->get();
        // dd($currentKategoriPertanyaan);
        foreach ($currentKategoriPertanyaan as $value) {
            $deletedRows = Kategori_pertanyaan::where('id', $value->id)->delete();
        }

        foreach ($request->all() as $key => $value) {
            // dd($value);
            if($key != "_token" && $key != "_method" && $key != "judul" && $key != "pertanyaan"){
                $kategoriPertanyaan = new Kategori_pertanyaan;
 
                $kategoriPertanyaan->tanya_id = $tanya->id;
                $kategoriPertanyaan->kategori_id = $value;
                
                $kategoriPertanyaan->save();
            }
        }

        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Edit Pertanyaan";
        $aktifitas->deskripsi = $request->judul;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();
        // menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data Berhasil diperbaharui');

        return redirect('/tanya');
    }

    public function destroy($id)
    {
        $kategoriPertanyaan = Kategori_pertanyaan::where('tanya_id', $id)->get();
        foreach ($kategoriPertanyaan as $item) {
            $item->delete();
        }
        $tanya = Tanya::find($id);
        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Delete Pertanyaan";
        $aktifitas->deskripsi = $tanya->judul;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();
        $tanya->delete();


        // menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data Pertanyaan berhasil dihapus');

        return redirect('/tanya');
    }
}
