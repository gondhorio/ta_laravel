<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use App\Kategori;
use App\Kategori_pertanyaan;
use App\Aktifitas;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kategori::all();
        return view('kategori.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kategori.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namakategori' => 'required',
        ]);

        $kategori = new Kategori;

        $kategori->namakategori = $request->namakategori;
        $kategori->tglbuat = date('YmdHis');
        $kategori->save();

        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Create Kategori";
        $aktifitas->deskripsi = $request->namakategori;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();

        // menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data kategori berhasil ditambahkan');

        // Kategori::create([
    	// 	'namakategori' => $request->namakategori,
    	// 	'tglbuat' => date('YmdHis'),
		// 	'isdel' => '0',
		// 	'user_id' => $request->user_id
    	// ]);

 
    	return redirect('/kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.show', compact('kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('kategori.edit', compact('kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'namakategori' => 'required',
        ]);

        //$kategori = Kategori::find($id);
	
		$kategori = Kategori::where('id',$id)->update([
    		'namakategori' => $request->namakategori,
    		'tglbuat' => date('YmdHis')
    	]);
		
		//$kategori->namakategori = $request->namakategori;
        //$kategori->tglbuat = date('YmdHis');
        // $kategori->isdel = 0;
        // $kategori->user_id = $request->user_id;
       // $kategori->save();
 

        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Edit Kategori";
        $aktifitas->deskripsi = $request->namakategori;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();

		// menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data kategori berhasil diperbaharui');

        return redirect('/kategori');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategoriPertanyaan = Kategori_pertanyaan::where('kategori_id', $id)->get();
        foreach ($kategoriPertanyaan as $item) {
            $item->delete();
        }
        $kategori = Kategori::find($id);
        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Delete Kategori";
        $aktifitas->deskripsi = $kategori->namakategori;
        $aktifitas->user_id = Auth::id();

        $aktifitas->save();
        $kategori->delete();


        // menampilkan pesan berhasil
        Alert::success('Yeaaayy', 'Data kategori berhasil dihapus');

        return redirect('/kategori');
    }
}
