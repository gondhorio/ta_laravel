<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Tanya;
use App\Kategori_pertanyaan;
use App\User;
use App\Jawab;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {		
		$ListKategori = Kategori::all();
		$ListPertanyaan = Kategori_pertanyaan::
			Join('tanya', 'tanya.id', '=', 'kategori_pertanyaan.tanya_id')
			->Join('users', 'users.id', '=', 'tanya.user_id')			
			->select('tanya.*','users.name')
			//->where('kategori_pertanyaan.kategori_id', 2)
			->orderBy('tanya.tgljam','desc')->take(10)
			->get();
		
		$kolomKategori = []; $kolomJml = [];
		foreach ($ListPertanyaan as $idx=>$nilai) {				
			$InKategori = Kategori_pertanyaan::
				join('kategori', 'kategori.id', '=', 'kategori_pertanyaan.kategori_id')
				->select('kategori.namakategori')
				->where('kategori_pertanyaan.tanya_id', $nilai->id)
				->get();
																	
			foreach ($InKategori as $key=>$value) {						
				$kolomKategori[$nilai->id][$key] = $value;
			}	
			
			$kolomJml[$nilai->id] = Jawab::
				select('jawab.jawaban')
				->where('jawab.tanya_id', $nilai->id)
				->get()->count();		
		}
		
		//dd($kolomKategori);
	
        return view('admin', compact('ListKategori', 'ListPertanyaan', 'kolomKategori', 'kolomJml'));
    }
	
	public function searchKategori($id)
    {        			
		/*	
		$InKategori = Kategori_pertanyaan::where('kategori_id', $id)->pluck('tanya_id');		
		$dataFilter = [];
		foreach ($InKategori as $key=>$value) {
			$dataFilter[$key] = $value;
		}
		*/				
		//$ListPertanyaan = Tanya::where('id',$dataFilter)->get();
		
		$ListKategori = Kategori::all();
		$ListPertanyaan = Kategori_pertanyaan::
			Join('tanya', 'tanya.id', '=', 'kategori_pertanyaan.tanya_id')
			->Join('users', 'users.id', '=', 'tanya.user_id')			
			->select('tanya.*','users.name')
			->where('kategori_pertanyaan.kategori_id', $id)
			->get();			
		
		$kolomKategori = []; $kolomJml = [];
		foreach ($ListPertanyaan as $idx=>$nilai) {				
			$InKategori = Kategori_pertanyaan::
				join('kategori', 'kategori.id', '=', 'kategori_pertanyaan.kategori_id')
				->select('kategori.namakategori')
				->where('kategori_pertanyaan.tanya_id', $nilai->id)
				->get();
																	
			foreach ($InKategori as $key=>$value) {						
				$kolomKategori[$nilai->id][$key] = $value;
			}	
			
			$kolomJml[$nilai->id] = Jawab::
				select('jawab.jawaban')
				->where('jawab.tanya_id', $nilai->id)
				->get()->count();	
		}
		
		//dd($kolomKategori);
		
		$cat = "Kategori " . Kategori::find($id)->namakategori;
        return view('admin', compact('ListKategori', 'ListPertanyaan', 'cat', 'kolomKategori', 'kolomJml'));
    }
	
	public function searchByName(Request $request)
    {        					
		
		$txtCari = $request->txtCari;
		$column = "tanya.judul";
		
		$ListKategori = Kategori::all();
		$ListPertanyaan = Kategori_pertanyaan::
			Join('tanya', 'tanya.id', '=', 'kategori_pertanyaan.tanya_id')
			->Join('users', 'users.id', '=', 'tanya.user_id')			
			->select('tanya.*','users.name')
			//->where('tanya.judul', $id)
			//->orWhere('tanya.judul', 'ilike', '%' . $txtCari . '%')
			->whereRaw("UPPER({$column}) LIKE '%". strtoupper($txtCari)."%'")
			->get();			
		
		$kolomKategori = []; $kolomJml = [];
		foreach ($ListPertanyaan as $idx=>$nilai) {				
			$InKategori = Kategori_pertanyaan::
				join('kategori', 'kategori.id', '=', 'kategori_pertanyaan.kategori_id')
				->select('kategori.namakategori')
				->where('kategori_pertanyaan.tanya_id', $nilai->id)
				->get();
																	
			foreach ($InKategori as $key=>$value) {						
				$kolomKategori[$nilai->id][$key] = $value;
			}
			
			$kolomJml[$nilai->id] = Jawab::
				select('jawab.jawaban')
				->where('jawab.tanya_id', $nilai->id)
				->get()->count();			
		}
		
		//dd($kolomKategori);
		
        return view('admin', compact('ListKategori', 'ListPertanyaan', 'kolomKategori', 'kolomJml'));
    }
}
