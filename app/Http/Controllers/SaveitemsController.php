<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use RealRashid\SweetAlert\Facades\Alert;
use App\Tanya;
use App\Saveitems;
use App\Aktifitas;
use DB;

class SaveitemsController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function saveitem($id)
    {

        $saved = Saveitems::all();
        $found = false;

        foreach ($saved as $key => $value) {
            if($value->user_id == Auth::id() && $value->tanya_id == $id){
                $found = true;
            }
        }

        if(!$found){

            $tanya = Tanya::find($id);
            $saveItem = new Saveitems;
            $saveItem->user_id = Auth::id();
            $saveItem->tanya_id = $id;
    
            $saveItem->save();

            $aktifitas = new Aktifitas;
            $aktifitas->tgljam = date('YmdHis');
            $aktifitas->nama_aktifitas = "Save Pertanyaan";
            $aktifitas->deskripsi = $tanya->judul;
            $aktifitas->user_id = Auth::id();
    
            $aktifitas->save();

            Alert::success('Yeaaayy', 'Data pertanyaan berhaasil disimpan');
        }else{
            
            Alert::success('Data sudah tersimpan', 'Data pertanyaan gagal disimpan');
        }
        return redirect('tanya/'.$id);


    }

    public function index(){
        $save = Saveitems::where('user_id', Auth::id())->get();
        $tanya = array();
        foreach ($save as $item) {
            $tanya[] = Tanya::find($item->tanya_id);
        }
        $kategori = DB::table('kategori_pertanyaan')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->join('tanya', 'tanya_id', '=', 'tanya.id')
            ->select('kategori_pertanyaan.*', 'kategori.*', 'tanya.*')
            ->get();

        return view('saveitem.index', compact('tanya', 'kategori'));
    }
}
