<?php

namespace App\Http\Controllers;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Kategori;
use App\Tanya;
use App\Kategori_pertanyaan;
use App\User;
use App\Jawab;
use App\Respond;
use Auth;

class ShowAskController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    function index($id){	
		
		$ListKategori = Kategori::all();
		$ListPertanyaan = Tanya::
			Join('users', 'users.id', '=', 'tanya.user_id')	
			->join('profile', 'profile.user_id', '=', 'users.id')
			//->Join('kategori_pertanyaan', 'kategori_pertanyaan.tanya_id', '=', 'tanya.id')		
			->select('tanya.*','users.name','profile.avatar')
			->where('tanya.id', $id)			
			->get();			
		
		$kolomKategori = ""; $jmlJawab = 0;
		foreach ($ListPertanyaan as $idx=>$nilai) {				
			$InKategori = Kategori_pertanyaan::
				join('kategori', 'kategori.id', '=', 'kategori_pertanyaan.kategori_id')
				->select('kategori.namakategori')
				->where('kategori_pertanyaan.tanya_id', $nilai->id)
				->get();
																	
			foreach ($InKategori as $key=>$value) {						
				$kolomKategori = $kolomKategori . $value->namakategori . " | ";
			}
			
			$jmlJawab = Jawab::
				select('jawab.jawaban')
				->where('jawab.tanya_id', $nilai->id)
				->get()->count();	
		}
		
		$ListJawaban = Jawab::
			join('users', 'users.id', '=', 'jawab.user_id')
			->join('profile', 'profile.user_id', '=', 'users.id')
			->select('jawab.*','users.name','profile.avatar')
			->where('jawab.tanya_id', $id)		
			->orderBy('jawab.issolutions','desc')					
			->get();
		
		$xxx = "";
		$kolomHelpfull = [];
		$kolomNotHelpfull = [];
		foreach ($ListJawaban as $idx=>$nilai) {				
			$ok = Respond::
				select('respond.*')
				->where([['respond.jid', $nilai->id],['respond.helpfull', 1]])
				->get()->count();
						
			$not = Respond::
				select('respond.*')
				->where([['respond.jid', $nilai->id],['respond.helpfull', 0]])
				->get()->count();	
																										
			$kolomHelpfull[$nilai->id.$nilai->tanya_id.$nilai->user_id] = $ok;
			$kolomNotHelpfull[$nilai->id.$nilai->tanya_id.$nilai->user_id] = $not;
		}
		
		return view('showask', compact('ListKategori', 'ListPertanyaan', 'ListJawaban', 'kolomKategori', 'jmlJawab', 'kolomHelpfull', 'kolomNotHelpfull'));
	}
	
	function helpfull(Request $request){
		$iserlogin = Auth::id(); //Auth::user()->id;
		$rid = $request->rid;
		$uid = $request->uid;
		$askid = $request->askid;
		$st = $request->st;

		$Cek = Respond::select('respond.*')
			->where([ ['respond.jawab_user_id', $uid], ['respond.jawab_tanya_id', $askid], ['respond.user_id', $iserlogin] ])			
			->get()->count();
		
		if ($Cek == 0){
			Respond::insert(
				['jid' => $rid, 'helpfull' => $st, 'jawab_user_id' => $uid, 'jawab_tanya_id' => $askid, 'user_id' => $iserlogin]
			);
		}else{
			Respond::where([ ['respond.jawab_user_id', $uid], ['respond.jawab_tanya_id', $askid], ['respond.user_id', $iserlogin], ['jid',$rid] ] )->update(['helpfull'=>$st]);
		}
		
		return redirect("/showask/".$request->askid);
	}
	
	function setclose(Request $request){
		$askid = $request->askid;
		$jid = $request->jid;
		
		Jawab::where('id',$jid)->update(['issolutions'=>'1']);
		Tanya::where('id',$askid)->update(['isclosed'=>'1']);
		Alert::success('Yeaaayy', 'Pertanyaan telah di tutup');
		return redirect("/showask/".$request->askid);
	}
	
	function setjawaban(Request $request){		
		Jawab::insert(
				['user_id' => Auth::id(), 'tanya_id' => $request->tanyaid, 'tgljam' => date('YmdHis'), 'jawaban' => $request->txtjawaban, 'issolutions' => '0']
			);
		
		Alert::success('Yeaaayy', 'Jawaban berhasil ditambahkan');
		return redirect("/showask/".$request->tanyaid);
	}
}
