<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Tanya;
use App\Jawab;
use App\kategori;
use App\Aktifitas;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function show(){
        $tanya = Tanya::where('user_id', Auth::id())->get();
        $jawab = Jawab::first();
        $jawab_all = Jawab::get();
        $kategoris = DB::table('kategori_pertanyaan')
            ->join('kategori', 'kategori_id', '=', 'kategori.id')
            ->join('tanya', 'tanya_id', '=', 'tanya.id')
            ->select('kategori_pertanyaan.*', 'kategori.*', 'tanya.*')
            ->get();

        // History user
        $aktifitas = new Aktifitas;
        $aktifitas->tgljam = date('YmdHis');
        $aktifitas->nama_aktifitas = "Print Report";
        $aktifitas->deskripsi = Auth::user()->name;
        $aktifitas->user_id = Auth::id();
        $aktifitas->save();

        // Print PDF
        $pdf = PDF::loadView('pdf.show',compact('tanya', 'jawab', 'jawab_all', 'kategoris'));
        return $pdf->stream('');
    }
}
