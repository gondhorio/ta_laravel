<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori_pertanyaan extends Model
{
    protected $table = "kategori_pertanyaan";
    protected $fillable = ["tanya_id", "kategori_id"];
	public $timestamps = false;
	
	public function tanya()
  	{
   		return $this->belongsToMany('App\Tanya');
  	} 
	 
  	public function kategori()
  	{
   		return $this->belongsToMany('App\Kategori');
  	}
  
}
